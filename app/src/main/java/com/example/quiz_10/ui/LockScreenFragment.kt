package com.example.quiz_10.ui

import android.util.Log.d
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.quiz_10.Password
import com.example.quiz_10.Type
import com.example.quiz_10.databinding.FragmentLockScreenBinding
import com.example.quiz_10.extensions.DRAWABLES
import com.example.quiz_10.extensions.STRINGS
import com.example.quiz_10.extensions.showSnackBar
import com.example.quiz_10.ui.base.BaseFragment
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class LockScreenFragment :
    BaseFragment<FragmentLockScreenBinding>(FragmentLockScreenBinding::inflate) {
    private lateinit var buttonAdapter: ButtonAdapter
    private val viewModel: ButtonViewModel by viewModels()

    override fun init() {
        initRV()
        observer()
        reloadPinView(0)
    }

    private fun initRV() {
        binding.rvBtns.apply {
            buttonAdapter = ButtonAdapter().apply {
                clickCallBack = {
                    clickBtn(it)
                }
            }
            adapter = buttonAdapter
            layoutManager =
                GridLayoutManager(view?.context , 3)
        }
        buttonAdapter.setData(viewModel.buttons)

    }

    private fun observer() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.password.collectLatest { password ->
                if (password.length < 4) {
                    reloadPinView(password.length)
                } else {
                    if (password == Password.PASSWORD) {
                        binding.root.showSnackBar(getString(STRINGS.success))
                    } else {
                        binding.root.showSnackBar(getString(STRINGS.try_again))
                    }
                    viewModel.deleteString()
                }
            }
        }
    }

    private fun clickBtn(name: String) =
        when (name) {
            Type.FingerPrint.toString() -> {
                binding.root.showSnackBar(getString(STRINGS.does_not_work_yet))
            }
            Type.BackPrint.toString() -> {
                viewModel.backPrint()
            }
            else -> {
                viewModel.plusString(name)
            }
        }

    private fun reloadPinView(pinSize: Int) {
        binding.pinView.removeAllViews()
        (1..getMaxPinSize()).forEach {
            val imageView = ImageView(this.context)
            val layoutParams = LinearLayout.LayoutParams(25 , 25 , 0.0f)
            layoutParams.setMargins(25 , 25 , 25 , 25)
            imageView.layoutParams = layoutParams
            if (it > pinSize) {
                imageView.setImageResource(DRAWABLES.dot_empty)
            } else {
                imageView.setImageResource(DRAWABLES.dot_fill)
            }
            binding.pinView.addView(imageView)
        }
    }

    private fun getMaxPinSize(): Int = 4

}