package com.example.quiz_10.ui

import androidx.lifecycle.ViewModel
import com.example.quiz_10.Type
import com.example.quiz_10.extensions.DRAWABLES
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow


class ButtonViewModel : ViewModel() {

    private var _password = MutableStateFlow<String>("")
    val password = _password.asStateFlow()

    fun plusString(string: String) {
        _password.value += string
    }

    fun deleteString() {
        _password.value = ""
    }

    fun backPrint() {
        val string = _password.value
        _password.value = string.dropLast(1)
    }

    val buttons = mutableListOf(
        Button("1" , null) ,
        Button("2" , null) ,
        Button("3" , null) ,
        Button("4" , null) ,
        Button("5" , null) ,
        Button("6" , null) ,
        Button("7" , null) ,
        Button("8" , null) ,
        Button("9" , null) ,
        Button(Type.FingerPrint.toString() , DRAWABLES.ic_finger_print) ,
        Button("0" , null) ,
        Button(Type.BackPrint.toString() , DRAWABLES.ic_back)
    )

}