package com.example.quiz_10.ui

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_10.Type
import com.example.quiz_10.databinding.LayoutBtnBinding
import com.example.quiz_10.databinding.LayoutBtnNumberBinding
import com.example.quiz_10.extensions.STRINGS


typealias ClickCallBack = (name: String) -> Unit

class ButtonAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val ICON_ITEM = 10
        private const val NUMBER_ITEM = 11
    }
    private val buttons = mutableListOf<Button>()
    var clickCallBack: ClickCallBack? = null

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int) : RecyclerView.ViewHolder =
        if (viewType == ICON_ITEM) {
            IconViewHolder(
                LayoutBtnBinding.inflate(
                    LayoutInflater.from(parent.context) ,
                    parent ,
                    false
                )
            )
        }else {
            NumberViewHolder(
                LayoutBtnNumberBinding.inflate(
                    LayoutInflater.from(parent.context) ,
                    parent ,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder , position: Int) {
        if (holder is IconViewHolder) {
            holder.onBind(buttons[position])
        } else if (holder is NumberViewHolder) {
            holder.onBind(buttons[position])
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (buttons[position].drawable != null) ICON_ITEM else NUMBER_ITEM

    override fun getItemCount() = buttons.size

    inner class IconViewHolder(private val binding: LayoutBtnBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(btn: Button) {
            with(binding) {
                btnClick.setImageResource(btn.drawable!!)
                if(btn.name == Type.FingerPrint.toString()){
                    btnClick.setColorFilter(Color.GREEN)
                }
            }
            binding.root.setOnClickListener {
                clickCallBack?.invoke(btn.name)
            }
        }
    }

    inner class NumberViewHolder(private val binding: LayoutBtnNumberBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(btn: Button) {
            with(binding) {
                btnClick.text = btn.name
            }
            binding.root.setOnClickListener {
                clickCallBack?.invoke(btn.name)
            }
        }
    }

    fun setData(buttonsList: MutableList<Button>) {
        buttons.clear()
        buttons.addAll(buttonsList)
        notifyDataSetChanged()
    }
}

