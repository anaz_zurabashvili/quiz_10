package com.example.quiz_10

enum class Type {
    FingerPrint, BackPrint
}